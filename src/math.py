from test_project_2.abstr.abstraction import Abstraction


class Math:

    def sum(self, number_1, number_2):
        return number_1.get() + number_2.get()

    def do_something(self, text):
        text = Abstraction(text)
        return text.get_text()
